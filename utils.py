import logging
import os
import re
import time
import random
import genanki
import zipfile
import pandas as pd
from tqdm import tqdm
import shutil
from urllib.request import urlretrieve
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager


def get_browser():
    options = Options()
    options.add_argument("-headless")
    browser = webdriver.Chrome(
        service=Service(ChromeDriverManager().install()), options=options
    )
    return browser


def generate_anki_deck():
    uniq_deck_id = random.randint(0, 99999)
    my_deck = genanki.Deck(uniq_deck_id, f"Quiz oiseaux {uniq_deck_id}")
    return my_deck


def generate_simple_anki_model():
    uniq_model_id = random.randint(0, 99999)
    anki_model = genanki.Model(
        uniq_model_id,
        "Simple Model",
        fields=[
            {"name": "Answer"},
            {"name": "MyMedia"},
        ],
        templates=[
            {
                "name": "Card",
                "qfmt": "{{MyMedia}}",
                "afmt": '{{FrontSide}}<hr id="answer">{{Answer}}',
            },
        ],
    )
    return anki_model


def get_list_img_from_INPN(browser, CD_nom_oiseau):
    URL_INPN = "https://inpn.mnhn.fr/espece/cd_nom/"
    browser.get(URL_INPN + str(CD_nom_oiseau))
    time.sleep(3)
    images = browser.find_elements(By.TAG_NAME, "img")
    images = [img.get_attribute("src") for img in images if img is not None]
    list_images_link = [
        img
        for img in images
        if img is not None and "https://inpn.mnhn.fr/photos/uploads/webtofs/inpn" in img
    ]
    return list_images_link


def get_list_sound_from_INPN(browser, anki_model, CD_nom_oiseau):
    URL_INPN = "https://inpn.mnhn.fr/espece/cd_nom/"
    browser.get(URL_INPN + str(CD_nom_oiseau))
    time.sleep(1)
    html_content = browser.find_elements(By.TAG_NAME, "body")[0].get_attribute(
        "innerHTML"
    )
    link_sonds = re.findall(
        "https://sonotheque.mnhn.fr/api/sounds/.{1,64}/stream", html_content
    )
    return link_sonds


def create_anki_img_note(oiseau, anki_model, list_images_link):
    random_image = (
        "<img src='https://live.staticflickr.com/4693/39145824952_373020d3ea_c.jpg' onload='this.onload=null; this.src=getImagePath();' />  <script> function getImagePath() {  var choices = "
        + str(list_images_link)
        + "; var index = Math.floor(Math.random() * choices.length); return choices[index];  } </script>"
    )
    anki_img_note = genanki.Note(
        model=anki_model,
        fields=[
            oiseau,
            random_image,
        ],
    )
    return anki_img_note


def create_anki_sound_note(oiseau, anki_model, link_sonds):
    all_sound_players = ""
    for one_link_sound in link_sonds:
        one_player = (
            '<audio controls> \
            <source src="'
            + one_link_sound
            + '" type="audio/mpeg"> \
            Your browser does not support the audio element. \
            </audio> '
        )
        all_sound_players += one_player
    anki_sound_note = genanki.Note(
        model=anki_model,
        fields=[
            oiseau,
            all_sound_players,
        ],
    )
    return anki_sound_note


def get_taxref_file():
    taxref_filepath = "data/TAXREFv16.txt"
    if not os.path.exists(taxref_filepath):
        download_taxref()
    df_taxref = pd.read_csv(taxref_filepath, sep="\t", low_memory=False)
    return df_taxref


def download_taxref():
    logging.info(
        "Downloding taxref file, use to find CD_NOM associated with a species name"
    )
    URL_taxref = "https://inpn.mnhn.fr/docs-web/docs/download/413554"
    TMP_FOLDER = "tmp/"
    if not os.path.exists(TMP_FOLDER):
        os.mkdir(TMP_FOLDER)
    path_to_zip_file = TMP_FOLDER + "taxref.zip"
    urlretrieve(URL_taxref, filename=path_to_zip_file)
    with zipfile.ZipFile(path_to_zip_file, "r") as zip_ref:
        zip_ref.extractall(TMP_FOLDER)
    os.rename("tmp/TAXREFv16.txt", "data/TAXREFv16.txt")
    shutil.rmtree(TMP_FOLDER)


def generate_dict_CD_nom(df_taxref):
    dico_taxref = dict()
    for _, row in tqdm(df_taxref.iterrows(), total=len(df_taxref)):
        if (
            not isinstance(row["NOM_VERN"], float)
            and len(row["LB_NOM"].split(" ")) == 2
        ):
            for possibility in row["NOM_VERN"].split(","):
                dico_taxref[possibility.strip()] = row["CD_NOM"]
    return dico_taxref


def verifiy_bird_list(liste_oiseaux, dico_cd_nom):
    logging.info("Verifying bird names")
    some_unkown = False
    for oiseau in liste_oiseaux:
        if oiseau not in dico_cd_nom.keys():
            logging.info(f"{oiseau} is unkown in the database")
            some_unkown = True
    if some_unkown:
        raise ValueError(f"Some bird name are unkown in the database")


def save_deck(data_type, bird_deck, output_folder="output_deck/"):
    if not os.path.exists(output_folder):
        os.mkdir(output_folder)
    if data_type == "image":
        output_file_name = output_folder + "bird_photos_quiz.apkg"
    elif data_type == "sound":
        output_file_name = output_folder + "bird_sond_quiz.apkg"
    else:
        raise ValueError("data_type unkown")

    genanki.Package(bird_deck).write_to_file(output_file_name)
