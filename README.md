# Anki bird quiz generator
Description: Generate [anki](https://ankiweb.net) card deck to learn to identify bird species from image or sound

Remark: only French species name for now

Tested with Python 3.11.6

## Install necessary library
```
python3 -m pip -r requirements.txt
```

## Generate image cards
```
python3 generate_anki_bird_quiz.py 'data/liste_canards.txt','data/liste_oiseaux_debutant.txt','data/liste_oiseaux_intermediaire.txt','data/liste_oiseaux_avance.txt','data/liste_oiseaux_marins.txt','data/liste_rapaces.txt' -d image
```
For the image quiz, one image is shown randomly each time you see it, this is to avoid recognizing only the specific photos and learn the criteria to identify the bird species.

You can choose only a subset of these files if you want or create your own file.

### Examples
- Hirondelle

<img src="examples/hirondelle_photo_question.png" alt="Hirondelle photo question" width="48%"/>
<img src="examples/hirondelle_photo_answer.png" alt="Hirondelle photo question" width="48%"/>

- Martinet

<img src="examples/martinet_photo_question.png" alt="Martinet photo question" width="48%"/>
<img src="examples/martinet_photo_answer.png" alt="Martinet photo question" width="48%"/>

## Generate sound cards
```
python3 generate_anki_bird_quiz.py 'data/liste_canards.txt','data/liste_oiseaux_debutant.txt','data/liste_oiseaux_intermediaire.txt','data/liste_oiseaux_avance.txt','data/liste_oiseaux_marins.txt','data/liste_rapaces.txt' -d sound
```
### Examples
- Tourterelle

<img src="examples/tourterelle_sound_question.png" alt="Tourterelle photo question" width="48%"/>
<img src="examples/tourterelle_sound_answer.png" alt="Tourterelle photo question" width="48%"/>
