import logging
import argparse
import pandas as pd
from tqdm import tqdm

from utils import (
    create_anki_img_note,
    create_anki_sound_note,
    generate_anki_deck,
    generate_dict_CD_nom,
    generate_simple_anki_model,
    get_browser,
    get_list_img_from_INPN,
    get_list_sound_from_INPN,
    get_taxref_file,
    save_deck,
    verifiy_bird_list,
)

parser = argparse.ArgumentParser(
    prog="Anki_bird_quiz_generator",
)
parser.add_argument("filelist", help="List of file with bird name seperated by a comma")
parser.add_argument("-d", "--data_type", choices=["image", "sound"])
args = parser.parse_args()
logging.basicConfig(level=logging.INFO)

# Load the browser
browser = get_browser()

### 1- Create the anki model
anki_model = generate_simple_anki_model()

### 2- Get bird list and all CD_nom
filelist = args.filelist.split(",")
logging.info(f"List of files: {filelist}")
liste_oiseaux = []
for filename in filelist:
    fichier_oiseaux = open(filename, "r")
    liste_oiseaux_this_file = fichier_oiseaux.readlines()
    liste_oiseaux += liste_oiseaux_this_file
liste_oiseaux = [oiseau.replace("\n", "") for oiseau in liste_oiseaux]
logging.info(f"Number of bird to deal with: {len(liste_oiseaux)} species")

logging.info("Generating dictionary of CD_nom and species name with taxref file")
df_taxref = get_taxref_file()
dico_cd_nom = generate_dict_CD_nom(df_taxref)

verifiy_bird_list(liste_oiseaux, dico_cd_nom)

### 3- Get image and generate card for each bird
logging.info("Generating anki card")
bird_deck = generate_anki_deck()
card_list = []
for oiseau in tqdm(liste_oiseaux):
    logging.info(oiseau)
    CD_nom_oiseau = dico_cd_nom[oiseau]
    if args.data_type == "image":
        list_images_link = get_list_img_from_INPN(browser, CD_nom_oiseau)
        card = create_anki_img_note(oiseau, anki_model, list_images_link)
    elif args.data_type == "sound":
        link_sonds = get_list_sound_from_INPN(browser, anki_model, CD_nom_oiseau)
        if len(link_sonds) == 0:
            continue
        card = create_anki_sound_note(oiseau, anki_model, link_sonds)
    else:
        raise ValueError("Unkown data_type")
    bird_deck.add_note(card)

logging.info("Saving of anki card deck to a file")
save_deck(args.data_type, bird_deck)
